(function($, window, document, undefined) {

	// Grid
	$.fn.grid = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				itemSelector: '.item',
				imageSelector: 'img',
				waitForImages: false,
				horizontalMargin: 0,
				verticalMargin: 0,
				onItemReady: false,
				onReady: false
			},
			settings = $.extend(defaults, options),
			addRow = function(grid, columns) {
				var row = [];

				for(var i = 0; i < columns; i++) {
					row.push(false);
				}

				grid.push(row);
			},
			addColumn = function(grid) {
				for(var i = 0, length = grid.length; i < length; i++) {
					grid[i].push(false);
				}
			},
			arrangeItems = function(items, availableWidth, container) {
				var currentX = 0,
					currentY = 0,
					currentRow = 0,
					currentColumn = 0,
					containerWidth = 0,
					containerHeight = 0,
					maxColumns = 1,
					grid = [],
					i, length, item, width, height;

				for(i = 0, length = items.length; i < length; i++) {
					item = items.eq(i);
					width = item.outerWidth(false);
					height = item.outerHeight(false);

					if(width <= 0 || height <= 0 || (settings.waitForImages && !item.data('imageReady'))) {
						continue;
					}

					if(currentX + width > availableWidth) {
						addRow(grid, maxColumns);

						currentRow++;
						currentColumn = 0;
						currentX = 0;
					}

					currentY = currentRow > 0 ? (grid[currentRow - 1][currentColumn].position().top + grid[currentRow - 1][currentColumn].outerHeight(false) + settings.verticalMargin) : 0;

					item.css({
						left: currentX + 'px',
						top: currentY + 'px'
					});

					if(currentX + width > containerWidth) {
						containerWidth = currentX + width;
					}
					if(currentY + height > containerHeight) {
						containerHeight = currentY + height;
					}

					if(!grid[currentRow]) {
						addRow(grid, maxColumns);
					}
					if(grid[currentRow].length <= currentColumn) {
						addColumn(grid);
						maxColumns++;
					}

					grid[currentRow][currentColumn] = item;

					currentX += width + settings.horizontalMargin;
					currentColumn++;

					if(settings.onItemReady) {
						settings.onItemReady(item);
					}
				}

				if(containerWidth > 0 && containerHeight > 0) {
					container.css({
						minWidth: containerWidth + 'px',
						minHeight: containerHeight + 'px'
					});
				}

				if(settings.onReady) {
					settings.onReady();
				}
			};

		return this.each(function() {
			var container = $(this),
				availableWidth = container.width(),
				items = container.find(settings.itemSelector),
				onImageLoad = function() {
					$(this).closest(settings.itemSelector).data('imageReady', true);
					arrangeItems(items, availableWidth, container);
				},
				images = 0,
				item, image;

			if(!items.length) {
				if(settings.onReady) {
					settings.onReady();
				}

				return;
			}

			if(settings.waitForImages) {
				for(var i = 0, length = items.length; i < length; i++) {
					item = items.eq(i);
					image = item.find(settings.imageSelector).first();

					if(image.length && !image.prop('complete')) {
						images++;
						image.on('load', onImageLoad);
					}
					else {
						item.data('imageReady', true);
					}
				}

				if(!images) {
					arrangeItems(items, availableWidth, container);
				}
			}
			else {
				arrangeItems(items, availableWidth, container);
			}
		});
	};

	// Watch video button
	$.fn.productVideoButton = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				cancelButtonSelector: '.product-gallery-thumbnail',
				videoContainerSelector: '.video-container',
				playingClass: 'playing-video',
				containerSelector: '.gallery',
				iframeHeight: 0
			},
			settings = $.extend(defaults, options),
			container = $(settings.containerSelector),
			cancelButton = container.find(settings.cancelButtonSelector);

		if(cancelButton.length && !cancelButton.data('isCancelVideoButton')) {
			cancelButton.data('isCancelVideoButton', true).on('click', function() {
				if(!container.hasClass(settings.playingClass)) {
					return;
				}

				container.removeClass(settings.playingClass).find(settings.videoContainerSelector).html('');
			});
		}

		return this.on('click', function(event) {
			event.preventDefault();
			if(container.hasClass(settings.playingClass)) {
				return;
			}

			var videoContainer = container.find(settings.videoContainerSelector),
				width = parseInt(videoContainer.width(), 10),
				height = settings.iframeHeight ? settings.iframeHeight : parseInt(width / 1.5, 10),
				iframe = '<iframe width="' + width + '" height="' + height + '" src="' + this.href + '" frameborder="0" allowfullscreen></iframe>';

			container.addClass(settings.playingClass);
			videoContainer.html(iframe);
			$(document).scrollTop(0);
		});
	};

	// Sort drop down
	$.fn.productSortDropDown = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				headerSelector: '.header',
				dropDownSelector: '.drop-down',
				defaultText: 'Please select',
				openClass: 'open'
			},
			settings = $.extend(defaults, options);

		return this.each(function() {
			var container = $(this),
				header = container.find(settings.headerSelector),
				dropDown = container.find(settings.dropDownSelector),
				links = container.find('a');

			header.html(links.filter('.current').html() || settings.defaultText);
			links.on('click', function() {
				dropDown.toggle();
				header.toggleClass(settings.openClass).html($(this).html());
			});
			header.on('click', function() {
				header.toggleClass(settings.openClass);
				dropDown.toggle();
			});
		});
	};

	// Fixed element
	$.fn.fixedElement = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				top: 0,
				container: false,
				bottomFix: true,
				keepPosition: true,
				variableContainerHeight: false
			},
			settings = $.extend(defaults, options),
			$document = $(document),
			handlers = [],
			containerBottom = 0,
			resetCss = {
				position: '',
				marginTop: '',
				marginBottom: '',
				top: '',
				bottom: '',
				left: '',
				right: ''
			},
			i, top, length,
			onScroll = function() {
				top = $document.scrollTop();

				for(i = 0; i < length; i++) {
					handlers[i](top);
				}
			};

		if(typeof settings.container === 'string') {
			settings.container = $(settings.container);
		}

		if(settings.container) {
			settings.container.css('min-height', settings.container.height() + 'px');

			if(settings.bottomFix) {
				containerBottom = settings.container.offset().top + settings.container.outerHeight(false);
			}
		}

		this.each(function() {
			var element = $(this),
				elementTop = element.offset().top,
				elementHeight = element.outerHeight(false);

			handlers.push(function(documentTop) {
				if(documentTop >= elementTop - settings.top) {
					if(!element.data('fixed')) {
						element.data('fixed', true).css({
							top: settings.top + 'px',
							left: settings.keepPosition ? (element.offset().left + 'px') : '',
							right: 'auto',
							bottom: 'auto',
							position: 'fixed',
							marginTop: 0
						});
					}
					else if(containerBottom) {
						if(settings.variableContainerHeight) {
							containerBottom = settings.container.offset().top + settings.container.outerHeight(false);
						}

						if(documentTop + settings.top + elementHeight >= containerBottom) {
							if(!element.data('fixed-bottom')) {
								element.data('fixed-bottom', true).css({
									top: 'auto',
									left: settings.keepPosition ? '' : (element.offset().left + 'px'),
									right: settings.keepPosition ? '' : 'auto',
									bottom: 0,
									position: 'absolute',
									marginBottom: 0
								});
							}
						}
						else if(element.data('fixed-bottom')) {
							element.data('fixed-bottom', false).css({
								top: settings.top + 'px',
								left: settings.keepPosition ? (element.offset().left + 'px') : '',
								right: 'auto',
								bottom: 'auto',
								position: 'fixed',
								marginTop: 0
							});
						}
					}
				}
				else if(element.data('fixed')) {
					element.data({
						fixed: false,
						fixedBottom: false
					}).css(resetCss);
				}
			});
		});

		length = handlers.length;
		$document.on('scroll', onScroll);
		onScroll();

		return this;
	};

	// Tabs
	$.fn.tabsContainer = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				triggerSelector: '.common-tab-trigger',
				contentSelector: '.common-tab-content',
				currentClass: 'current',
				visibleClass: 'visible'
			},
			settings = $.extend(defaults, options);

		return this.each(function() {
			var container = $(this),
				triggers = container.find(settings.triggerSelector),
				tabs = container.find(settings.contentSelector);

			triggers.on('click', function() {
				var index = triggers.index(this);
				if(index === -1) {
					return;
				}

				triggers.filter('.' + settings.currentClass).removeClass(settings.currentClass);
				$(this).addClass(settings.currentClass);
				tabs.filter('.' + settings.visibleClass).removeClass(settings.visibleClass);

				var content = tabs.eq(index);
				content.addClass(settings.visibleClass);

				container.triggerHandler('change', [content]);
			});
		});
	};

	// Quantity box
	$.fn.quantityBox = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				upClass: 'up',
				downClass: 'down',
				numberClass: 'number',
				min: 0,
				submitForm: false
			},
			settings = $.extend(defaults, options),
			onButtonClick = function() {
				var $this = $(this),
					input = $this.data('input'),
					number = $this.data('number'),
					current = parseInt(input.val(), 10);

				if($this.hasClass(settings.downClass)) {
					if(current <= settings.min) {
						return;
					}

					current--;
				}
				else {
					current++;
				}

				input.val(current).trigger('change');
				number.text(current);

				if(settings.submitForm) {
					$this.closest('form').trigger('submit');
				}
			};

		return this.each(function() {
			var box = $(this),
				up = box.find('.' + settings.upClass),
				down = box.find('.' + settings.downClass),
				data = {
					number: box.find('.' + settings.numberClass),
					input: box.find('input')
				};

			up.data(data).on('click', onButtonClick);
			down.data(data).on('click', onButtonClick);
		});
	};

	// Address form
	$.fn.addressForm = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				sourceFields: [],
				destinationFields: [],
				destinationContainer: false,
				checkBox: false
			},
			settings = $.extend(defaults, options),
			checkBox = settings.checkBox,
			container = settings.destinationContainer;

		if(typeof checkBox === 'string') {
			checkBox = $(settings.checkBox);
		}
		if(typeof container === 'string') {
			container = $(settings.destinationContainer);
		}

		if(!checkBox || !checkBox.length) {
			return this;
		}

		checkBox.on('change', function() {
			if(!container || !container.length) {
				return;
			}

			var checked = this.checked;
			if(checked) {
				container.slideUp();
			}
			else {
				container.slideDown();
			}
		});

		return this.on('submit', function() {
			if(checkBox && checkBox.prop('checked')) {
				var source, destination;

				for(var i = 0, length = Math.min(settings.sourceFields.length, settings.destinationFields.length); i < length; i++) {
					source = typeof settings.sourceFields[i] === 'string' ? $(settings.sourceFields[i]) : settings.sourceFields[i];
					destination = typeof settings.destinationFields[i] === 'string' ? $(settings.destinationFields[i]) : settings.destinationFields[i];
					destination.val(source.val());
				}
			}
		});
	};

	// List with drop down
	$.fn.listWithDropDown = function(options) {
		// ignore empty collections
		if(!this.length) {
			return this;
		}

		var defaults = {
				dropDownSelector: '.subcategories',
				triggerSelector: '.category',
				itemSelector: 'li',
				markerClass: 'with-subcategories',
				animate: false
			},
			settings = $.extend(defaults, options);

		var triggerClick = function(event) {
			var $this = $(this),
				item = $this.data('item'),
				dropDown = $this.data('dropDown');

			event.preventDefault();
			if(dropDown.is(':visible')) {
				if(settings.animate) {
					dropDown.slideUp();
				}
				else {
					dropDown.hide();
				}

				item.removeClass('active');
			}
			else {
				if(settings.animate) {
					dropDown.slideDown();
				}
				else {
					dropDown.show();
				}

				item.addClass('active');
			}
		};

		return this.find(settings.triggerSelector).each(function() {
			var item = $(this).closest(settings.itemSelector),
				dropDown = item.find(settings.dropDownSelector);

			if(dropDown.length) {
				item.addClass(settings.markerClass);

				if(item.hasClass('open')) {
					item.addClass('active');
					dropDown.show();
				}

				$(this).data({
					item: item,
					dropDown: dropDown
				}).on('click', triggerClick);
			}
		});
	};

	// Custom check box
	$.fn.customCheckBox = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				radioBoxesContainer: '.radio-boxes',
				checkedClass: 'checked'
			},
			settings = $.extend(defaults, options),
			onInputChange = function() {
				if(this.checked) {
					if(this.type === 'radio') {
						var radioBoxes = $(this).closest(settings.radioBoxesContainer).find('input[type="radio"]');
						for(var i = 0; i < radioBoxes.length; i++) {
							radioBoxes.eq(i).parent().removeClass(settings.checkedClass);
						}
					}

					$(this).parent().addClass(settings.checkedClass);
				}
				else {
					$(this).parent().removeClass(settings.checkedClass);
				}
			};

		return this.on('click', function() {
			var input = $(this).children('input').first(),
				radio = input.prop('type') === 'radio';

			if(radio) {
				input.prop('checked', true).trigger('change');
			}
			else {
				input.prop('checked', input.prop('checked') ? false : true).trigger('change');
			}
		}).each(function() {
			var input = $(this).children('input').first();

			input.on('change', onInputChange);
			if(input.prop('checked')) {
				input.trigger('change');
			}
		});
	};

	// Field with placeholder
	$.fn.fieldWithPlaceholder = function(options) {
		// ignore empty collections
		if(!this.length) {
			return this;
		}

		var defaults = {
				placeholderSelector: '.placeholder'
			},
			settings = $.extend(defaults, options);

		var onPlaceholderClick = function() {
			$(this).parent().children('input, textarea').focus();
		};

		return this.on('focus', function() {
			var $this = $(this);

			if(!$this.data('changed')) {
				$this.parent().children(settings.placeholderSelector).hide();
			}
		}).on('blur', function() {
			var $this = $(this);

			if(!$this.data('changed') || !$this.val()) {
				$this.parent().children(settings.placeholderSelector).css('display', 'block');
			}
		}).on('change', function() {
			var $this = $(this);
			$this.data('changed', $this.val() ? 1 : 0);
		}).each(function() {
			var $this = $(this),
				placeholder = $this.parent().children(settings.placeholderSelector);

			if($this.val()) {
				$this.data('changed', 1);
				placeholder.hide();
			}

			placeholder.on('click', onPlaceholderClick);
		});
	};

	// Image zoomer
	$.fn.imageZoomer = function(options) {
		// ignore empty collections
		if(!this.length) {
			return this;
		}

		var defaults = {
				squareWidth: 100,
				squareHeight: 100,
				overlayWidth: 450,
				overlayHeight: 450,
				overlayLeft: 10,
				doNotCheckImageWidth: false
			},
			settings = $.extend(defaults, options),
			overlay = $('#image-zoomer-overlay'),
			image = overlay.children('img'),
			firstSetup = true;

		return this.each(function() {
			var $this = $(this),
				sourceImage = $this.find('img');

			if(!sourceImage.length) {
				return;
			}

			var originalSourceImageWidth = sourceImage.width(),
				originalSourceImageHeight = sourceImage.height(),
				realWidth = sourceImage.css({ width: 'auto', height: 'auto' }).width();

			if(!settings.doNotCheckImageWidth && realWidth < settings.overlayWidth * 2) {
				return;
			}

			sourceImage.width(originalSourceImageWidth).height(originalSourceImageHeight);
			$this.addClass('hasImageZoomer');

			if(firstSetup) {
				firstSetup = false;

				if(!overlay.length || !image.length) {
					overlay = $('<div />');
					overlay.attr('id', 'image-zoomer-overlay').css({
						position: 'absolute',
						overflow: 'hidden',
						zIndex: 50,
						display: 'none',
						width: settings.overlayWidth + 'px',
						height: settings.overlayHeight + 'px',
						background: '#fff'
					}).appendTo(document.body);

					var inner = $('<div />');
					inner.addClass('inner').appendTo(overlay);

					image = $('<img />');
					image.css({
						display: 'block',
						position: 'absolute',
						maxWidth: 'none'
					}).appendTo(inner);
				}
			}

			var square = $('<div />');
			square.css({
				position: 'absolute',
				display: 'none',
				width: settings.squareWidth + 'px',
				height: settings.squareHeight + 'px',
				background: 'rgba(255, 255, 255, 0.5)',
				boxShadow: '0 0 1px rgba(0, 0, 0, 0.5)'
			}).addClass('image-zoomer-square').appendTo($this);

			$this.css('position', 'relative');

			var squareSize = square.outerWidth(),
				halfSize = parseInt(squareSize / 2, 10),
				parentWidth = $this.outerWidth(),
				parentHeight = $this.outerHeight(),
				parentOffset = $this.offset(),
				largeImageWidth = 0,
				largeImageHeight = 0,
				largeParentWidth = 0,
				largeParentHeight = 0,
				sourceImageWidth = originalSourceImageWidth,
				sourceImageHeight = originalSourceImageHeight,
				sourceImagePosition = sourceImage.position(),
				positionSquare = function(x, y) {
					x -= halfSize;
					y -= halfSize;

					if(x < 0) {
						x = 0;
					}
					else if(x > parentWidth - squareSize) {
						x = parentWidth - squareSize;
					}

					if(y < 0) {
						y = 0;
					}
					else if(y > parentHeight - squareSize) {
						y = parentHeight - squareSize;
					}

					var imageLeft = parseInt(((x - sourceImagePosition.left) / parentWidth) * largeParentWidth * -1, 10),
						imageTop = parseInt(((y - sourceImagePosition.top) / parentHeight) * largeParentHeight * -1, 10);

					square.css({
						'left': x + 'px',
						'top': y + 'px'
					});
					image.css({
						'left': imageLeft + 'px',
						'top': imageTop + 'px'
					});
				};

			image.on('load', function() {
				overlay.css('opacity', 1);
				sourceImageWidth = sourceImage.width();
				sourceImageHeight = sourceImage.height();
				sourceImagePosition = sourceImage.position();

				var width = (parentWidth / settings.squareWidth) * sourceImageWidth;
				image.width(width);

				largeImageWidth = image.width();
				largeImageHeight = image.height();
				largeParentWidth = (largeImageWidth / sourceImageWidth) * parentWidth;
				largeParentHeight = (largeImageHeight / sourceImageHeight) * parentHeight;
			});

			$this.on('mouseenter', function(event) {
				positionSquare(event.pageX - parentOffset.left, event.pageY - parentOffset.top);
				var src = $this.find('img').attr('src'),
					left = parentOffset.left + parentWidth + settings.overlayLeft;

				if(settings.overlayLeft < 0) {
					left = parentOffset.left - settings.overlayWidth - (settings.overlayLeft * -1);
				}

				if(image.attr('src') != src) {
					overlay.css({
						'left': left + 'px',
						'top': (parentOffset.top) + 'px',
						'opacity': 0
					});

					image.attr('src', src);
				}

				overlay.show();
				square.show();

			}).on('mouseleave', function() {
				square.hide();
				overlay.hide();
			}).on('mousemove', function(event) {
				positionSquare(event.pageX - parentOffset.left, event.pageY - parentOffset.top);
			});

			square.on('click', function() {
				sourceImage.trigger('click');
			});
		});
	};

	// Gallery thumbnails
	$.fn.productGallery = function(options) {
		// ignore empty collections
		if(!this.length) {
			return this;
		}

		var defaults = {
				thumbnailSelector: '.product-gallery-thumbnail',
				photoSelector: '.product-gallery-photo'
			},
			settings = $.extend(defaults, options),
			collection = this.find(settings.thumbnailSelector);

		// show photo after load
		var photo = $(settings.photoSelector);

		return this.on('click', settings.thumbnailSelector, function(event) {
			event.preventDefault();
			if(photo.prop('src') !== this.href) {
				// hide photo
				photo.css('opacity', 0);

				// load the new photo
				photo.prop('src', this.href);

				collection.filter('.current').removeClass('current');
				$(this).addClass('current');
			}
		});
	};

	// Product filter
	$.fn.productFilter = function(options) {
		// ignore empty collections
		if(!this.length) {
			return this;
		}

		var defaults = {
				loaderId: 'product-filter-loader',
				loaderImageUrl: '',
				containerSelector: '',
				params: '',
				currentPage: 1,
				currentSort: '',
				appliedOptions: [],
				customOptions: {},
				baseUrl: location.href,
				afterLoad: false,
				adjacentPageLinks: 4,
				pageNumberSelector: '',
				alwaysShowPageLinks: false
			},
			settings = $.extend(defaults, options),
			loader = $('#' + settings.loaderId);

		// construct loader if needed
		if(!loader.length) {
			loader = $('<div />');
			loader.prop('id', settings.loaderId).css({
				top: Math.floor(($(window).height() - 260) / 2),
				left: Math.floor(($(window).width() - 260) / 2),
				zIndex: 9999,
				width: '260px',
				height: '260px',
				display: 'none',
				position: 'absolute',
				lineHeight: '260px',
				textAlign: 'center',
				background: 'rgba(255, 255, 255, 0.6)'
			});

			// add image if specified
			if(settings.loaderImageUrl) {
				var loaderImage = $('<img />');

				loaderImage
					.css({
						opacity: 0,
						display: 'inline-block',
						verticalAlign: 'middle'
					})
					.on('load', function() {
						var left = Math.floor(($(window).width() - this.width) / 2),
							top = Math.floor(($(window).height() - this.width) / 2);

						// center and show image
						loaderImage.css({
							left: left + 'px',
							top: top + 'px',
							opacity: 1
						});
					})
					.appendTo(loader)
					.prop('src', settings.loaderImageUrl);
			}

			loader.appendTo(document.body);
		}

		var buttons = this,
			tempUrl = new URI(),
			query = tempUrl.search(true),
			pageNumber = settings.pageNumberSelector ? $(settings.pageNumberSelector) : false,
			pageNumberMax = pageNumber && pageNumber.length ? pageNumber.data('max') : 0;

		if(query.page) {
			settings.currentPage = parseInt(query.page, 10);
			buttons.filter('.filter_' + settings.currentPage).addClass('current');
		}
		if(query.sort) {
			settings.currentSort = query.sort;
			buttons.filter('.filter_' + settings.currentSort).addClass('current');
		}

		if(pageNumber && pageNumber.length) {
			pageNumber.text(settings.currentPage);

			if(!settings.alwaysShowPageLinks) {
				if(settings.currentPage <= 1) {
					buttons.filter('.previous-page').hide();
				}
				if(settings.currentPage >= pageNumberMax) {
					buttons.filter('.next-page').hide();
				}
			}
		}

		if(settings.adjacentPageLinks) {
			var pageButtons = buttons.filter('.filter_' + settings.currentPage),
				start = settings.currentPage - settings.adjacentPageLinks,
				end = settings.currentPage + settings.adjacentPageLinks,
				pageCount = false,
				temp;

			if(start <= 0) {
				start = 1;
				end = start + settings.adjacentPageLinks * 2;
			}

			for(var i = 0; i < pageButtons.length; i++) {
				temp = pageButtons.eq(i).parent().children('.page');

				if(!pageCount) {
					pageCount = temp.length;

					if(end > pageCount) {
						end = pageCount;
						start = end - settings.adjacentPageLinks * 2;

						if(start <= 0) {
							start = 1;
						}
					}
				}

				temp.removeClass('current').hide().slice(start - 1, end).show();
			}

			buttons.filter('.filter_' + settings.currentPage).addClass('current');
		}

		return buttons.on('click change', function(event) {
			if(this.type === 'checkbox' && event.type === 'click') {
				return;
			}

			var button = $(this),
				url = new URI(settings.baseUrl),
				currentFilter = button.data('filter'),
				previousFilter = $.isNumeric(currentFilter) ? settings.currentPage : settings.currentSort,
				sortValue = $.isNumeric(currentFilter) ? settings.currentSort : currentFilter,
				pageValue = $.isNumeric(currentFilter) ? currentFilter : settings.currentPage;

			if(this.type !== 'checkbox') {
				event.preventDefault();

				// check if current button is a previous/next button
				if(button.hasClass('previous-page')) {
					previousFilter = pageValue;
					pageValue -= 1;
					currentFilter = pageValue;
					sortValue = settings.currentSort;
				}
				if(button.hasClass('next-page')) {
					previousFilter = pageValue;
					pageValue++;
					currentFilter = pageValue;
					sortValue = settings.currentSort;
				}

				var pageLinkExists = buttons.filter('.filter_' + pageValue).length;
				if(pageNumber && pageNumber.length) {
					pageLinkExists = pageValue > 0 && pageValue <= pageNumberMax;
				}

				// make sure the filter exists and it's not currently selected
				if((!pageLinkExists && !buttons.filter('.filter_' + sortValue).length) || (sortValue == settings.currentSort && pageValue == settings.currentPage)) {
					return;
				}

				if((button.hasClass('previous-page') || button.hasClass('next-page')) && !pageLinkExists) {
					return;
				}

				if(settings.adjacentPageLinks) {
					var pageButtons = buttons.filter('.filter_' + pageValue),
						start = pageValue - settings.adjacentPageLinks,
						end = pageValue + settings.adjacentPageLinks,
						pageCount = false,
						temp;

					if(start <= 0) {
						start = 1;
						end = start + settings.adjacentPageLinks * 2;
					}

					for(i = 0; i < pageButtons.length; i++) {
						temp = pageButtons.eq(i).parent().children('.page');

						if(!pageCount) {
							pageCount = temp.length;

							if(end > pageCount) {
								end = pageCount;
								start = end - settings.adjacentPageLinks * 2;

								if(start <= 0) {
									start = 1;
								}
							}
						}

						temp.removeClass('current').hide().slice(start - 1, end).show();
					}
				}

				// mark current filter
				buttons.filter('.filter_' + previousFilter).removeClass('current');
				buttons.filter('.filter_' + currentFilter).addClass('current');
			}
			else {
				currentFilter = button.val();
				sortValue = settings.currentSort;
				pageValue = settings.currentPage;

				if(button.data('custom-option')) {
					settings.customOptions[button.data('option-name')] = this.checked ? button.data('option-value') : '';
				}
				else {
					var index = -1;

					for(var i = 0, length = settings.appliedOptions.length; i < length; i++) {
						if(settings.appliedOptions[i] == currentFilter) {
							index = i;
							break;
						}
					}

					if(this.checked && index === -1) {
						settings.appliedOptions.push(currentFilter);
					}
					else if(!this.checked && index >= 0) {
						settings.appliedOptions.splice(index, 1);
					}
				}
			}

			// show loader
			var container = $(settings.containerSelector);
			loader.show().css('top', Math.floor(container.offset().top + (container.outerHeight() - 260) / 2) + 'px');

			// construct URL
			var currentButton = buttons.filter('.filter_' + currentFilter);
			if(currentButton.data('custom')) {
				url = new URI(currentButton.prop('href'));
				if(settings.params) {
					url.addSearch(settings.params);
				}
			}
			else {
				url.removeSearch('sort');
				url.removeSearch('page');
				url.removeSearch('filter_id[]');
				if(settings.params) {
					url.addSearch(settings.params);
				}
				if(sortValue) {
					url.addSearch({ 'sort': sortValue });
				}
				if(pageValue) {
					url.addSearch({ 'page': pageValue });
				}
				if(settings.appliedOptions.length) {
					url.addSearch('filter_id[]', settings.appliedOptions);
				}
				if(settings.customOptions) {
					for(var key in settings.customOptions) {
						if(settings.customOptions.hasOwnProperty(key) && settings.customOptions[key]) {
							url.addSearch(key, settings.customOptions[key]);
						}
					}
				}

				if(window.history && history.replaceState) {
					var currentUrl = new URI();

					currentUrl.removeSearch('sort').removeSearch('page');
					if(sortValue) {
						currentUrl.addSearch({ 'sort': sortValue });
					}
					if(pageValue) {
						currentUrl.addSearch({ 'page': pageValue });
					}

					window.history.replaceState(null, document.title, currentUrl.toString());
				}
			}

			// send request
			$(document).scrollTop(0);
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: url.toString(),
				success: function(data) {
					// replace content
					$(settings.containerSelector).html(data.html).find('.showired-image-container').imageContainer();

					// remember filter
					if(pageValue) {
						settings.currentPage = pageValue;

						if(pageNumber && pageNumber.length) {
							pageNumber.text(settings.currentPage);

							if(!settings.alwaysShowPageLinks) {
								if(settings.currentPage <= 1) {
									buttons.filter('.previous-page').hide();
								}
								else {
									buttons.filter('.previous-page').show();
								}

								if(settings.currentPage >= pageNumberMax) {
									buttons.filter('.next-page').hide();
								}
								else {
									buttons.filter('.next-page').show();
								}
							}
						}
					}
					if(sortValue) {
						settings.currentSort = sortValue;
					}

					if(settings.afterLoad) {
						settings.afterLoad.call();
					}
				},
				complete: function() {
					// hide loader
					loader.hide();
				}
			});
		});
	};

	// Image container
	$.fn.imageContainer = function() {
		if(!this.length) {
			return this;
		}

		var onLoad = function() {
			var image = $(this),
				container = image.parent();

			image.css({
				width: 'auto',
				height: 'auto',
				maxWidth: 'none'
			});

			var width = image.width(),
				height = image.height();

			if(width > 0 && height > 0) {
				var containerWidth = container.outerWidth(false),
					containerHeight = container.outerHeight(false),
					fit = container.data('fit'),
					scale = container.data('scale'),
					padding = container.data('padding'),
					sizeChanged = false;

				fit = parseInt(fit, 10);
				scale = parseInt(scale, 10);
				padding = parseInt(padding, 10);

				if(isNaN(fit)) {
					fit = 1;
				}
				if(isNaN(scale)) {
					scale = 0;
				}
				if(isNaN(padding)) {
					padding = 0;
				}

				if(fit) {
					if((width >= containerWidth && height >= containerHeight) || scale) {
						height = Math.floor((containerWidth / width) * height);
						width = containerWidth;

						if(height < containerHeight) {
							width = Math.floor((containerHeight / height) * width);
							height = containerHeight;
						}

						sizeChanged = true;
					}
				}
				else if(width >= containerWidth || height >= containerHeight) {
					if(width > containerWidth) {
						height = Math.floor((containerWidth / width) * height);
						width = containerWidth;
					}
					if(height > containerHeight) {
						width = Math.floor((containerHeight / height) * width);
						height = containerHeight;
					}

					sizeChanged = true;
				}

				if(sizeChanged) {
					var currentPadding = Math.min(containerWidth - width, containerHeight - height);
					if(currentPadding < 0) {
						currentPadding = 0;
					}
					if(padding > currentPadding) {
						var previousWidth = width;
						width -= (padding - currentPadding) * 2;
						height = (width / previousWidth) * height;
					}

					image.width(width).height(height);
				}

				var left = Math.floor((containerWidth - width) / 2),
					top = Math.floor((containerHeight - height) / 2);

				image.css({
					left: left + 'px',
					top: top + 'px',
					opacity: 1
				});
			}
		};

		return this.each(function() {
			var container = $(this),
				image = container.children('img').first(),
				initialized = container.data('imageContainerInitialized');

			if(!image.length || initialized) {
				if(initialized) {
					image.trigger('load');
				}

				return;
			}

			container.data('imageContainerInitialized', true);
			image.css('opacity', 0).on('load', onLoad);

			if(image.prop('complete')) {
				image.trigger('load');
			}
		}).on('update', function() {
			$(this).children('img').first().trigger('load');
		});
	};

	// AJAX form
	$.fn.ajaxForm = function(options) {
		// default settings
		var settings = {
			successMessage: "",
			alertContent: false
		};

		// custom settings
		if(options) {
			$.extend(settings, options);
		}

		return $(this).submit(function(event) {
			var $this = $(this),
				formHeight = $this.outerHeight(),
				button = $this.find("input[type=submit], input[type=image]"),
				values = {
					ajax: 1
				},
				element;

			// prevent default submit
			event.preventDefault();

			// disable the submit button
			button.attr("disabled", true);

			// read fields
			var elements = $this.find("input[type=text], textarea, select, input[type=hidden]");
			for(i = 0; i < elements.le1ngth; i++) {
				element = $(elements.get(i));
				values[element.attr("name")] = element.val();
			}

			// read checkboxes and radio buttons
			var checkboxes = $this.find("input[type=checkbox], input[type=radio]");
			for(var i = 0; i < checkboxes.length; i++) {
				element = $(checkboxes.get(i));

				// ignore unchecked
				if(!element.attr("checked")) {
					continue;
				}

				var name = element.attr("name");
				var pos = name.indexOf("[");

				// check if it belongs to a group
				if(pos != -1) {
					name = name.substr(0, pos);
					if(values[name] == undefined) {
						values[name] = [];
					}

					values[name].push(element.val());
				}
				else {
					values[name] = element.val();
				}
			}

			// post values
			$.post($this.attr("action"), values, function(data) {
				var successContainer = false;

				// enable the submit button
				button.attr("disabled", false);

				if(settings.alertContent && data) {
					alert(data);
					return;
				}

				// set up success message
				if(settings.successMessage) {
					var id = $this.prop('id') + '-success';

					$this.after('<p id="' + id + '" style="display: none">' + settings.successMessage + '</p>');
					successContainer = $('#' + id);
					successContainer.height(formHeight);
				}

				// hide the form
				$this.fadeOut(300, function() {
					if(successContainer) {
						successContainer.fadeIn(300);
					}
				});
			});
		});
	};

	// Link with confirmation
	$.fn.linkWithConfirmation = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				message: 'Confirm'
			},
			settings = $.extend(defaults, options);

		return this.on('click', function() {
			return confirm(settings.message);
		});
	};

	// Checkout address form
	$.fn.checkoutAddressForm = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				checkBoxSelector: '#checkout-same-address',
				useCustomSelect: true,
				simple: true,
				reverse: false
			},
			settings = $.extend(defaults, options);

		return this.each(function() {
			var form = $(this),
				checkBox = form.find(settings.checkBoxSelector);

			if(settings.simple) {
				checkBox.on('change', function() {
					if(this.checked) {
						if(settings.reverse) {
							$('#shipping_company').val($('#billing_company').val());
							$('#shipping_name').val($('#billing_name').val());
							$('#shipping_phone').val($('#billing_phone').val());
							$('#shipping_email').val($('#billing_email').val());
							$('#shipping_address1').val($('#billing_address1').val());
							$('#shipping_address2').val($('#billing_address2').val());
							$('#shipping_address3').val($('#billing_address3').val());
							$('#shipping_city').val($('#billing_city').val());
							$('#shipping_postcode').val($('#billing_postcode').val());
							$('#shipping_country').val($('#billing_country').val()).trigger('render');
							$('#shipping_state_id').val($('#billing_state_id').val()).trigger('render');
						}
						else {
							$('#billing_company').val($('#shipping_company').val());
							$('#billing_name').val($('#shipping_name').val());
							$('#billing_phone').val($('#shipping_phone').val());
							$('#billing_email').val($('#shipping_email').val());
							$('#billing_address1').val($('#shipping_address1').val());
							$('#billing_address2').val($('#shipping_address2').val());
							$('#billing_address3').val($('#shipping_address3').val());
							$('#billing_city').val($('#shipping_city').val());
							$('#billing_postcode').val($('#shipping_postcode').val());
							$('#billing_country').val($('#shipping_country').val()).trigger('render');
							$('#billing_state_id').val($('#shipping_state_id').val()).trigger('render');
						}
					}
				});
			}
			else {
				if(settings.reverse) {
					form.addressForm({
						sourceFields: ['#shipping_company', '#shipping_name', '#shipping_phone', '#shipping_email', '#shipping_address1', '#shipping_address2', '#shipping_address3', '#shipping_city', '#shipping_postcode', '#shipping_country'],
						destinationFields: ['#billing_company', '#billing_name', '#billing_phone', '#billing_email', '#billing_address1', '#billing_address2', '#billing_address3', '#billing_city', '#billing_postcode', '#billing_country'],
						destinationContainer: '.destination-container',
						checkBox: settings.checkBoxSelector
					});
				}
				else {
					form.addressForm({
						sourceFields: ['#billing_company', '#billing_name', '#billing_phone', '#billing_email', '#billing_address1', '#billing_address2', '#billing_address3', '#billing_city', '#billing_postcode', '#billing_country'],
						destinationFields: ['#shipping_company', '#shipping_name', '#shipping_phone', '#shipping_email', '#shipping_address1', '#shipping_address2', '#shipping_address3', '#shipping_city', '#shipping_postcode', '#shipping_country'],
						destinationContainer: '.destination-container',
						checkBox: settings.checkBoxSelector
					});
				}

				if(settings.useCustomSelect) {
					checkBox.on('change', function() {
						if(!this.checked) {
							$('.destination-container').find('select').trigger('render').filter(':not(.hasCustomSelect)').customSelect();
						}
					});
				}
			}
		});
	};

	$.validator.addMethod('emailtld', function(val, elem){
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if(!filter.test(val)) {
			return false;
		} else {
			return true;
		}
	}, 'Correct email suffix required');
	
	// Form with validation
	$.fn.formWithValidation = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				errorClass: 'error',
				errorElement: 'div'
			},
			settings = $.extend(defaults, options);

		return this.each(function() {
			$(this).validate({
				errorClass: settings.errorClass,
				errorElement: settings.errorElement,
				onkeyup: false,
				onblur: false,
				onfocusout: false,
				errorPlacement: function(label, element) {
					if(element.hasClass('hasCustomSelect')) {
						var dropDown = element.next().addClass(settings.errorClass);
						label.insertAfter(dropDown);
					}
					else {
						label.insertAfter(element);
					}
				}
			});
		});
	};
	
	

	// Info message
	$.fn.infoMessage = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				pause: 4400,
				animationSpeed: 400
			},
			settings = $.extend(defaults, options),
			bottom = '-' + this.outerHeight(false) + 'px',
			self = this;

		return this.css({
			opacity: 1,
			bottom: bottom
		}).animate({
			bottom: 0
		}, settings.animationSpeed).delay(settings.pause).animate({
			bottom: bottom
		}, settings.animationSpeed, function() {
			self.hide();
		});
	};

	// Basket delivery form
	$.fn.basketDeliveryForm = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				countrySelector: 'select.country',
				rateSelector: 'select.rate',
				rateContainerSelector: '.rates',
				rateOuterContainerSelector: '.rates-container',
				postUrl: '/checkout/basket/totals',
				subTotalSelector: '#sub-total',
				subTotalWithoutVatSelector: '#sub-total-without-vat',
				shippingSelector: '#shipping',
				shippingWithoutVatSelector: '#shipping-without-vat',
				pointsSelector: '#total-points',
				discountsSelector: '#discounts',
				discountSelector: '#discount',
				vatSelector: '#vat',
				salesTaxSelector: '#sales-tax',
				totalSelector: '#total'
			},
			settings = $.extend(defaults, options),
			country = this.find(settings.countrySelector),
			rate = this.find(settings.rateSelector);

		if(country.length && rate.length) {
			var container = this.find(settings.rateContainerSelector),
				outerContainer = settings.rateOuterContainerSelector ? this.find(settings.rateOuterContainerSelector) : false,
				onRateChange = function(data) {
					if(!data) {
						return;
					}

					if(data.rates) {
						var html = [];

						for(var i = 0, length = data.rates.length; i < length; i++) {
							html.push('<option value="' + data.rates[i].id + '"' + (data.rates[i].current ? ' selected="selected"' : '') + '>' + data.rates[i].name + ' ' + data.rates[i].price + '</option>');
						}

						if(html.length) {
							container.show();
							if(outerContainer) {
								outerContainer.show();
							}
							rate.html(html.join('')).trigger('render');

							if(rate.hasClass('custom-select') && !rate.hasClass('hasCustomSelect')) {
								rate.customSelect();
							}
						}
						else {
							container.hide();
							if(outerContainer) {
								outerContainer.hide();
							}
						}
					}

					$(settings.subTotalSelector).html(data.subTotal);
					$(settings.subTotalWithoutVatSelector).html(data.subTotalWithoutVat);
					$(settings.shippingSelector).html(data.shipping);
					$(settings.shippingWithoutVatSelector).html(data.shippingWithoutVat);
					$(settings.pointsSelector).html(data.rewardPoints);
					$(settings.discountSelector).html(data.discount);
					$(settings.discountsSelector).html(data.discounts.length ? data.discounts.join('') : '');
					$(settings.vatSelector).html(data.vat);
					$(settings.salesTaxSelector).html(data.salesTax);
					$(settings.totalSelector).html(data.total);
				};

			country.on('change', function() {
				$.post(settings.postUrl, { countryId: country.val(), returnRates: true }, onRateChange, 'JSON');
			});
			rate.on('change', function() {
				$.post(settings.postUrl, { countryId: country.val(), rateId: rate.val() }, onRateChange, 'JSON');
			});

			if(!rate.val()) {
				container.hide();
				if(outerContainer) {
					outerContainer.hide();
				}
			}
		}

		return this;
	};

	// Product form
	$.fn.productForm = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				containerSelector: '#product-container',
				optionSelector: 'select.product-option'
			},
			settings = $.extend(defaults, options),
			productOptions = this.find(settings.optionSelector),
			container = $(settings.containerSelector),
			self = this;

		if(productOptions.length) {
			var productOptionsCache = {},
				onOptionChange = function(data) {
					var optionIndex = data.nextOptionIndex;
					if(optionIndex) {
						var html = ['<option value="">Please select...</option>'];
						for(var i = 0, length = data.nextOptionValues.length; i < length; i++) {
							html.push('<option value="' + data.nextOptionValues[i].id + '">' + data.nextOptionValues[i].name + '</option>');
						}

						var option = productOptions.eq(optionIndex);
						option.html(html.join('')).trigger('update');
					}

					$('.product-sku-value').html(data.sku);
					$('.product-price-value').html(data.price);
					$('.product-sale-price-value').html(data.salePrice);
					$('.product-reward-points-value').html(data.rewardPoints);

					if(data.canBeAdded) {
						container.addClass('can-be-added');
					}
					else {
						container.removeClass('can-be-added');
					}

					if(data.inStock) {
						container.addClass('in-stock');
					}
					else {
						container.removeClass('in-stock');
					}

					if(data.hasSalePrice) {
						container.addClass('has-sale-price');
					}
					else {
						container.removeClass('has-sale-price');
					}

					if(data.rewardPoints > 0) {
						container.addClass('has-reward-points');
					}
					else {
						container.removeClass('has-reward-points');
					}

					if(data.photo != -1) {
						$('.product-gallery-thumbnail.p' + data.photo).trigger('click');
					}
				};

			productOptions.on('change', function() {
				var index = productOptions.index(this),
					alternative = $(this).data('alternative');

				if(!alternative) {
					productOptions.slice(index + 1).html('<option value="">Please select...</option>').val('').trigger('render');
				}

				var post = productOptions.serialize() + '&get_info=1';

				if(productOptionsCache[post]) {
					onOptionChange(productOptionsCache[post]);
				}
				else {
					$.post(self.action, post, function(data) {
						if(data) {
							productOptionsCache[post] = data;
							onOptionChange(data);
						}
					}, 'JSON');
				}
			});
		}

		return this;
	};

	// Product collection
	$.fn.productCollection = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				pageLinkSelector: '.page-link',
				pageNumberSelector: '.page-number',
				paginationContainerSelector: '.pagination-container',
				sortOptionSelector: '.sort-option',
				filterSelector: '.filter',
				priceRangeSelector: '.price-range',
				containerSelector: '.items',
				pageLinkCurrentClass: 'current',
				sortOptionCurrentClass: 'current',
				filterCurrentClass: 'current',
				priceRangeCurrentClass: 'current',
				loadingClass: 'loading',
				sortDropDownSelector: '.sort-drop-down',
				itemsPerPageDropDownSelector: '.items-per-page-drop-down',
				ajaxValue: 'partials/products',
				adjacentPageLinks: 4,
				infinite: false,
				afterLoad: false,
				beforeLoad: false,
				itemsPerRequest: 10,
				itemsToRefresh: 10,
				initializeImageContainers: true
			},
			settings = $.extend(defaults, options);

		return this.each(function() {
			var parent = $(this),
				reachedInfinity = false,
				container = parent.find(settings.containerSelector),
				containerOffset = container.offset(),
				containerHeight = container.outerHeight(false),
				itemCount = container.children().length,
				filters = settings.filterSelector ? parent.find(settings.filterSelector) : false,
				priceRanges = settings.priceRangeSelector ? parent.find(settings.priceRangeSelector) : false,
				sortOptions = settings.sortOptionSelector ? parent.find(settings.sortOptionSelector) : false,
				sortDropDown = settings.sortDropDownSelector ? parent.find(settings.sortDropDownSelector) : false,
				itemsPerPageDropDown = settings.itemsPerPageDropDownSelector ? parent.find(settings.itemsPerPageDropDownSelector) : false,
				state = {
					sort: '',
					itemsPerPage: '',
					page: 1,
					totalPages: Math.max(1, parseInt(parent.data('pages') || 0, 10)),
					keywords: '',
					filters: [],
					priceRange: '',
					count: 0
				},
				busy = false,
				currentQuery = (new URI()).search(true),
				refreshPageLinks = function() {
					if(settings.adjacentPageLinks) {
						var start = state.page - settings.adjacentPageLinks,
							end = state.page + settings.adjacentPageLinks,
							containers = parent.find(settings.paginationContainerSelector);

						if(start <= 0) {
							start = 1;
							end = start + settings.adjacentPageLinks * 2;
						}
						if(end > state.totalPages) {
							end = state.totalPages;
							start = end - settings.adjacentPageLinks * 2;

							if(start <= 0) {
								start = 1;
							}
						}

						for(var i = 0; i < containers.length; i++) {
							containers.eq(i).find(settings.pageLinkSelector).slice(1, -1).addClass('hidden').slice(start - 1, end).removeClass('hidden');
							containers.eq(i).find(settings.pageNumberSelector).text(state.page);
						}
					}
				},
				refresh = function(loadMore, sorting, itemsPerPage) {
					var url = new URI(),
						query = {};

					busy = true;

					// remove parameters
					url.removeSearch('sort').removeSearch('page').removeSearch('keywords').removeSearch('items_per_page');
					url.removeSearch('filters[]').removeSearch('price_range').removeSearch('count').removeSearch('ajax')

					if(itemsPerPage) {
						state.page = 1;
					}

					// construct query
					if(state.sort) {
						query.sort = state.sort;
					}
					if(state.itemsPerPage) {
						query.items_per_page = state.itemsPerPage;
					}
					if(state.page > 0) {
						query.page = state.page;
					}
					if(state.keywords) {
						query.keywords = state.keywords;
					}
					if(state.filters.length) {
						query['filters[]'] = state.filters;
					}
					if(state.priceRange) {
						query.price_range = state.priceRange;
					}
					if(settings.infinite) {
						if(loadMore) {
							query.count = itemCount + settings.itemsPerRequest + itemCount % settings.itemsPerRequest;
						}
						else if(sorting) {
							query.count = Math.max(itemCount, settings.itemsToRefresh);
						}
						else {
							query.count = settings.itemsToRefresh;
						}
					}

					url.addSearch(query);

					// push history state
					if(window.history && history.pushState) {
						window.history.pushState(null, document.title, url.toString());
					}

					url.addSearch({ ajax: settings.ajaxValue }).removeSearch('count').removeSearch('start');

					if(settings.infinite) {
						if(loadMore) {
							// load more items
							var count = settings.itemsPerRequest + itemCount % settings.itemsPerRequest;
							url.addSearch({ start: itemCount, count: count });
						}
						else {
							// replace items
							url.addSearch({ start: 0, count: sorting ? Math.max(itemCount, settings.itemsToRefresh) : settings.itemsToRefresh });

							// focus container
							container.html('');
							$(document).scrollTop(0);
						}
					}

					parent.addClass(settings.loadingClass);

					// call beforeLoad handler
					if(settings.beforeLoad) {
						settings.beforeLoad.call(null, {
							container: container
						});
					}

					// send request
					$.ajax({
						type: 'GET',
						dataType: 'JSON',
						url: url.toString(),
						success: function(data) {
							var i, length;

							if(settings.infinite) {
								if(data.html) {
									// append content
									container.append(data.html);
									itemCount = container.children().length;
									containerHeight = container.outerHeight(false);
								}
								else {
									reachedInfinity = true;

									// replace history state
									if(window.history && history.replaceState) {
										url.removeSearch('count').removeSearch('start').removeSearch('ajax').addSearch({ count: itemCount });
										window.history.replaceState(null, document.title, url.toString());
									}
								}
							}
							else {
								// replace content
								container.html(data.html);
							}

							if(settings.initializeImageContainers) {
								container.find('.shopwired-image-container').imageContainer();
							}

							// mark page links
							if(state.page) {
								var pageLinks = parent.find(settings.pageLinkSelector);
								if(pageLinks.length) {
									pageLinks.filter('.' + settings.pageLinkCurrentClass).removeClass(settings.pageLinkCurrentClass);
									pageLinks.filter('[data-page=' + state.page + ']').addClass(settings.pageLinkCurrentClass);

									// refresh page links
									var totalPages = parseInt(data.total_pages, 10),
										pageLink;
									if(totalPages > 0 && state.totalPages !== totalPages) {
										state.totalPages = totalPages;

										for(i = 0, length = pageLinks.length; i < length; i++) {
											pageLink = pageLinks.eq(i);
											if(pageLinks.eq(i).data('page') > totalPages) {
												pageLink.hide();
											}
											else {
												pageLink.show();
											}
										}
									}
								}

								refreshPageLinks();
							}

							// mark sort options
							if(sortOptions && sortOptions.length) {
								sortOptions.filter('.' + settings.sortOptionCurrentClass).removeClass(settings.sortOptionCurrentClass);
								if(state.sort) {
									sortOptions.filter('[data-value="' + state.sort + '"]').addClass(settings.sortOptionCurrentClass);
								}
							}

							// mark filters
							if(filters && filters.length) {
								filters.filter('.' + settings.filterCurrentClass).removeClass(settings.filterCurrentClass).filter('input').prop('checked', false).trigger('change');
								if(state.filters) {
									for(i = 0, length = state.filters.length; i < length; i++) {
										filters.filter('[data-value="' + state.filters[i] + '"]').addClass(settings.filterCurrentClass).filter('input').prop('checked', true).trigger('change');
									}
								}
							}

							// mark price ranges
							if(priceRanges && priceRanges.length) {
								priceRanges.filter('.' + settings.priceRangeCurrentClass).removeClass(settings.priceRangeCurrentClass).filter('input').prop('checked', false).trigger('change');
								if(state.priceRange) {
									priceRanges.filter('[data-value="' + state.priceRange + '"]').addClass(settings.priceRangeCurrentClass).filter('input').prop('checked', true).trigger('change');
								}
							}
						},
						complete: function() {
							// call afterLoad handler
							if(settings.afterLoad) {
								settings.afterLoad.call(null, {
									container: container
								});
							}

							parent.removeClass(settings.loadingClass);
							busy = false;
						}
					});
				};

			// read initial state
			if(currentQuery.sort) {
				state.sort = '' + currentQuery.sort;
			}
			if(currentQuery.items_per_page) {
				state.itemsPerPage = '' + currentQuery.items_per_page;
			}
			if(currentQuery.page > 0) {
				state.page = parseInt(currentQuery.page, 10);
			}
			if(currentQuery.keywords) {
				state.keywords = '' + currentQuery.keywords;
			}
			if(currentQuery.price_range) {
				state.priceRange = '' + currentQuery.price_range;
			}
			if(currentQuery.count > 0) {
				state.count = parseInt(currentQuery.count, 10);
			}
			if(currentQuery['filters[]'] && currentQuery['filters[]'].length) {
				var currentFilters = currentQuery['filters[]'];
				state.filters = typeof currentFilters === 'object' ? currentFilters : [currentFilters];
			}
			if(settings.infinite) {
				state.page = 0;
			}

			// set up page links
			if(settings.pageLinkSelector) {
				refreshPageLinks();

				parent.on('click', settings.pageLinkSelector, function(event) {
					event.preventDefault();
					if(busy) {
						return;
					}

					var link = $(this),
						page = link.data('page'),
						load = false;

					// determine page
					if(page === 'prev' && state.page > 1) {
						state.page -= 1;
						load = true;
					}
					else if(page === 'next' && state.page < state.totalPages) {
						state.page += 1;
						load = true;
					}
					else {
						page = parseInt(page, 10);
						if(page >= 1 && page <= state.totalPages && page !== state.page) {
							state.page = page;
							load = true;
						}
					}

					if(load) {
						refresh();
					}
				});
			}

			// set up sort options
			if(sortOptions && sortOptions.length) {
				sortOptions.on('click', function(event) {
					event.preventDefault();
					if(busy) {
						return;
					}

					var option = $(this),
						value = option.data('value');

					// change sort option
					if(value && value != state.sort) {
						state.sort = value;
						reachedInfinity = false;
						refresh(false, true);
					}
				});
			}

			if(sortDropDown && sortDropDown.length) {
				// set up sort drop down
				sortDropDown.on('change', function() {
					if(busy) {
						return;
					}

					var value = $(this).val();

					// change sort option
					if(value && value != state.sort) {
						state.sort = value;
						reachedInfinity = false;
						refresh(false, true);
					}
				});
			}

			if(itemsPerPageDropDown && itemsPerPageDropDown.length) {
				// set up items per page drop down
				itemsPerPageDropDown.on('change', function() {
					if(busy) {
						return;
					}

					var value = $(this).val();

					// change items per page
					if(value && value != state.itemsPerPage) {
						state.itemsPerPage = value;
						reachedInfinity = false;
						refresh(false, false, true);
					}
				});
			}

			// set up filters
			if(filters && filters.length) {
				filters.on('click change', function(event) {
					var filter = $(this),
						type = this.tagName.toLowerCase();

					if(type === 'a' && event.type === 'click') {
						event.preventDefault();
					}
					if(busy) {
						return;
					}

					if(!state.filters) {
						state.filters = [];
					}

					var value = type === 'input' ? filter.val() : filter.data('value'),
						index = state.filters.indexOf('' + value),
						load = false;

					// add or remove filter
					if((type === 'input' && this.checked) || (type !== 'input' && !filter.hasClass(settings.filterCurrentClass))) {
						if(index === -1) {
							state.filters.push('' + value);
							load = true;
						}
					}
					else if(index !== -1) {
						state.filters.splice(index, 1);
						load = true;
					}

					if(load) {
						reachedInfinity = false;
						refresh();
					}
				});
			}

			// set up price ranges
			if(priceRanges && priceRanges.length) {
				priceRanges.on('change click', function(event) {
					var range = $(this),
						type = this.tagName.toLowerCase();

					if(type === 'a' && event.type === 'click') {
						event.preventDefault();
					}
					if(busy) {
						return;
					}

					var value = range.data('value'),
						remove = (type === 'input' && !this.checked) || (type !== 'input' && state.priceRange == value);

					// set price range
					if(remove) {
						state.priceRange = '';
					}
					else {
						state.priceRange = value;
					}

					reachedInfinity = false;
					refresh();
				});
			}

			// set up scrolling
			if(settings.infinite) {
				var $document = $(document),
					$window = $(window);

				$document.on('scroll', function() {
					if(reachedInfinity || busy) {
						return;
					}

					if($document.scrollTop() + $window.height() >= containerOffset.top + containerHeight) {
						refresh(true);
					}
				});
			}
		});
	};

	// Product add to basket button
	$.fn.productAddToBasketButton = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				view: 'partials/basket_items',
				totalSelector: '.shopwired-basket-total-value',
				modalSelector: '.shopwired-basket-modal',
				error: function(message) {
					alert(message || 'An error occurred. Please try again later.');
				}
			},
			settings = $.extend(defaults, options),
			basketModal = false;

		if(settings.modalSelector) {
			var modal = $(settings.modalSelector);
			if(modal.length) {
				if(!modal.data('ready')) {
					var innerModal = modal.find('.inner');

					modal.on('show', function() {
						var windowHeight = $(window).height();

						modal.show();

						var top = $(document).scrollTop() + Math.max(25, Math.floor((windowHeight - innerModal.outerHeight()) / 2));

						innerModal.css({
							'marginTop': top + 'px'
						});

						modal.height(Math.max($(document).height(), windowHeight));

						innerModal.find('.shopwired-image-container').imageContainer();
					});

					innerModal.on('click', function(event) {
						event.stopPropagation();
					});

					modal.on('click', function() {
						modal.hide();
					});

					modal.find('.close-button').on('click', function() {
						modal.hide();
					});

					innerModal.on('click', '.remove-button', function(event) {
						event.preventDefault();
						$.post(this.href, { ajax: 1 }, function(data) {
							if(settings.totalSelector && data && data.total) {
								$(settings.totalSelector).html(data.total || 0);
							}
						}, 'JSON');
						$(this).closest('.item').remove();
					});

					modal.data('ready', true);
				}

				basketModal = modal;
			}
		}

		return this.on('click', function(event) {
			var button = $(this);

			event.preventDefault();
			if(button.data('busy')) {
				return;
			}

			button.data('busy', true);

			var form = button.closest('form'),
				formData = form.serialize() + '&ajax=1&view=' + settings.view;

			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: form.prop('action'),
				data: formData,
				success: function(data) {
					if(!data || !data.success) {
						if(settings.error) {
							settings.error.call(null, data.errors ? data.errors.join("\n") : '');
						}
						return;
					}

					if(settings.totalSelector) {
						$(settings.totalSelector).html(data.total || 0);
					}

					if(basketModal) {
						basketModal.find('.items').html(data.items || '');
						basketModal.trigger('show');
					}
				},
				error: function() {
					if(settings.error) {
						settings.error.call();
					}
				},
				complete: function() {
					button.data('busy', false);
				}
			});
		});
	};

	// Toggle button
	$.fn.toggleButton = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				buttonClass: 'active',
				toggleClass: 'hidden'
			},
			settings = $.extend(defaults, options);

		return this.on('click', function(event) {
			var button = $(this),
				toggle = button.data('toggle');

			event.preventDefault();
			button.toggleClass(settings.buttonClass);

			if(toggle) {
				$(toggle).toggleClass(settings.toggleClass);
			}
		});
	};

	$.fn.stateDropDown = function(options) {
		if(!this.length) {
			return this;
		}

		var defaults = {
				url: '/checkout/states',
				countrySelector: '.country-select',
				selectMessage: 'Select...',
				loadingMessage: 'Loading...',
				notApplicableMessage: 'Not Applicable'
			},
			settings = $.extend(defaults, options),
			countrySelect = $(settings.countrySelector),
			stateSelect = $(this);

		countrySelect.on('change', function() {
			var country = countrySelect.val();

			stateSelect.html('<option value="">' + (country ? settings.loadingMessage : settings.notApplicableMessage) + '</option>').trigger('render');

			if(country) {
				$.ajax({
					type: 'GET',
					url: settings.url,
					data: {
						country: country
					},
					dataType: 'JSON',
					success: function(data) {
						var html = [];

						if(data.states && data.states.length) {
							html.push('<option value="">' + settings.selectMessage + '</option>');
							for(var i = 0, length = data.states.length; i < length; i++) {
								html.push('<option value="' + data.states[i].id + '">' + data.states[i].name + '</option>');
							}
						}
						else {
							html.push('<option value="">' + settings.notApplicableMessage + '</option>');
						}

						stateSelect.html(html.join('')).trigger('render');
					}
				});
			}
		});

		return this;
	};

})(jQuery, window, document);
