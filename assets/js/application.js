function textLengthInPx(fontSize, fontFamily, text) {
	var canvas = document.createElement("canvas"),
		ctx = canvas.getContext("2d"),
		font = fontSize + " " + fontFamily;

	ctx.font = font;

	return ctx.measureText(text).width;
}

function initEllipsis(selector, maxLines) {
	$(selector).html(function(index, text) {
		var $element = $(this),
			containerWidth = $element.width();
			fontSize = $element.css("font-size"),
			fontFamily = $element.css("font-family").replace(/"/g, ""),
			textWidth = textLengthInPx(fontSize, fontFamily, text),
			textLengthRatio = textWidth / containerWidth;

		if(textLengthRatio > maxLines) {
			$(this).attr("title", text);

			var currentLength = text.length,
				textLength = currentLength * maxLines / textLengthRatio;

			return text.substr(0, textLength - 3) + "&hellip;";
		}
		else {
			return text;
		}
	});
}

function renderCustomCheckboxesAndRadioButtons() {
	var renderCustomCheckboxes = function() {
		$(".custom-checkbox").each(function(index, element) {
			var $checkbox = $(element).find("input");
			var $tick = $(element).find(".icon-toggle");

			if($checkbox.prop("checked")) {
				$tick.addClass("icon-checkbox");
				$tick.removeClass("icon-checkbox-not-checked");
			}
			else {
				$tick.removeClass("icon-checkbox");
				$tick.addClass("icon-checkbox-not-checked");
			}
		});
	};

	$(".custom-checkbox").on("click", function() {
		var $checkbox = $(this).children("input:checkbox");
		$checkbox.prop("checked", !$checkbox.prop("checked")).trigger("change");

		var $radio = $(this).children("input:radio");
		$radio.prop("checked", true).trigger("change");
	});

	$("input:checkbox").change(function() {
		renderCustomCheckboxes();
	});

	$("input:radio").change(function() {
		renderCustomCheckboxes();
	});

	renderCustomCheckboxes();
}


function initFeatures() {
	//$('.product-form').productForm();
	$('.custom-select').customSelect();
	$('.product-gallery').productGallery();
	$('.shopwired-info-message').infoMessage();
	$('.collection-container').productCollection();
	$('.basket-delivery-form').basketDeliveryForm();
	$('.form-with-validation').formWithValidation();
	$('.shopwired-image-container').imageContainer();
	$('.product-soft-add-button').productAddToBasketButton();
	$('.product-video-button').productVideoButton({ containerSelector: '#product-container' });
	$('.checkout-form').checkoutAddressForm({ checkBoxSelector: '.checkout-same-address', simple: false, reverse: true });
	$('.basket-remove-button').linkWithConfirmation({ message: 'Are you sure you wish to remove this item from your shopping basket?' });

	$('.gallery-photo').prettyPhoto({
		show_title: false,
		social_tools: ''
	});

	var productGalleryPhoto = $('.product-gallery-photo');
	if(productGalleryPhoto && productGalleryPhoto.data('lightbox')) {
		productGalleryPhoto.on('click', function() {
			$.prettyPhoto.open(this.src);
		});
	}

	var productGalleryMainPhoto = $('.product-gallery-main-photo');
	if(productGalleryMainPhoto.length && productGalleryMainPhoto.data('zoom')) {
		var productInfoContainer = $('#product-container').find('.product-right');

		productGalleryMainPhoto.imageZoomer({
			overlayWidth: 494,
			overlayHeight: 494,
			overlayLeft: 21,
			doNotCheckImageWidth: true
		}).on('mouseenter', function() {
			productInfoContainer.css('opacity', 0);
		}).on('mouseleave', function() {
			productInfoContainer.css('opacity', '');
		});
	}

	$(window).on('resize', $.throttle(300, false, function() {
		$('.custom-select').trigger('render');
		$('.shopwired-image-container').trigger('update');
	}));
}

$(function() {
	$('.banner-slider').slick({
		autoplay: true,
		autoplaySpeed: 4000,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		dots: true,
		arrows: false
	});

	$('.new-products-slider').slick({
		autoplay: true,
		autoplaySpeed: 4000,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		dots: false,
		prevArrow: "<span class='slide-arrow slide-left-arrow'><span class='icon-secondleftarrow'></span></span>",
		nextArrow: "<span class='slide-arrow slide-right-arrow'><span class='icon-secondrightarrow'></span></span>",
		appendArrows: $(".products-slider-container h2"),
		responsive: [
			{
				breakpoint: 790,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 670,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 450,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.related-products-slider').slick({
		autoplay: true,
		autoplaySpeed: 4000,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		dots: false,
		prevArrow: "<span class='slide-arrow slide-left-arrow'><span class='icon-secondleftarrow'></span></span>",
		nextArrow: "<span class='slide-arrow slide-right-arrow'><span class='icon-secondrightarrow'></span></span>",
		appendArrows: $(".products-slider-container h2"),
		responsive: [
			{
				breakpoint: 790,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 670,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 450,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.product-thumb-slider').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		prevArrow: "<span class='slider-arrow slider-arrow-left'><span class='icon-secondleftarrow'></span></span>",
		nextArrow: "<span class='slider-arrow slider-arrow-right'><span class='icon-secondrightarrow'></span></span>",
		responsive: [
			{
				breakpoint: 490,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}
		]
	});

	$(".search-show").hide();
	$("#search-show").click(function() {
		$(".search-hide").hide();
		$(".search-show").show();
	});

	$("#search-hide").click(function() {
		$(".search-show").hide();
		$(".search-hide").show();
	});

	$("#menu-trigger").click(function() {
		$("#user-menu-collapsed").slideToggle();
	});

	$(".hide-reviews").hide();
	$(".reviews-visible").hide();

	$(".view-reviews").click(function(e) {
		e.preventDefault();

		$(this).hide();
		$(".hide-reviews").show();
		$(".reviews-visible").show();
		$(".reviews-hidden").hide();
		renderCustomSelect();
	});

	$(".hide-reviews").click(function(e) {
		e.preventDefault();

		$(this).hide();
		$(".view-reviews").show();
		$(".reviews-hidden").show();
		$(".reviews-visible").hide();
		renderCustomSelect();
	});

	$(".sidebar").css("height", $(".content-container").height() + 10);

	renderCustomCheckboxesAndRadioButtons();

	initEllipsis(".headline-ellipsis", 1);

	initFeatures();
	

	
	
});
