(function($, window, document, undefined) {

	$.fn.optionChange = function(data){

		let container = $(this).closest('#product-container');
		// if more options for this product
						
		if(data.nextOptionIndex && data.nextOptionValues.length) {
						let optionObj = container.find('ul.product-option').eq(data.nextOptionIndex);
						optionObj.html('');
						$.each(data.nextOptionValues, function(i, val) {
							$('<li>').val(val.id).html('<span>'+val.name+'</span>').appendTo(optionObj).on('click', function(){
								$(this).onOptionClick();
							});
						});		
						
						
					}					
					(data.sku) ? container.find('.product-sku-value').html(data.sku) : '';
					(data.price) ? container.find('.product-price-value').html(data.price) : '';
					(data.salePrice) ? container.find('.product-sale-price-value').html(data.salePrice) : '';
					(data.rewardPoints) ? container.find('.product-reward-points-value').html(data.rewardPoints) : '';

					if(data.canBeAdded) {
						container.addClass('can-be-added');
					}
					else {
						container.removeClass('can-be-added');
					}

					if(data.inStock) {
						container.addClass('in-stock');
					}
					else {
						container.removeClass('in-stock');
					}

					if(data.hasSalePrice) {
						container.addClass('has-sale-price');
					}
					else {
						container.removeClass('has-sale-price');
					}

					if(data.rewardPoints > 0) {
						container.addClass('has-reward-points');
					}
					else {
						container.removeClass('has-reward-points');
					}

					if(data.photo != -1) {
						$('.product-gallery-thumbnail.p' + data.photo).trigger('click');
					}		
					
	};
	
	$.fn.selectOptions = function() {			
		// form object
		let form = $(this);
		
		//let post_string = form.find('input.product-option').serialize() + '&get_info=1';	
		let post_string = '';
		form.find('input.product-option').each(function(i){
			post_string += ($(this).val()) ? '&' + $(this).attr('name') + '=' + $(this).val() : '';
		});
		post_string += '&get_info=1';	
		// get cache
		let optionsCache = form.data('options-cache') || {};
		//let optionsCache = {};
		
		// get data from cache or ajax call
		if(optionsCache[post_string]) {
			form.optionChange(optionsCache[post_string]);
		} else {
			$.post(form[0].action, post_string, function(data) {
				if(data) {
					optionsCache[post_string] = data;
					form.data('options-cache', optionsCache);
					form.optionChange(data);
				}
			}, 'JSON');		
		}
										
		return this;
	};
	

	// option choose buttons	
	$.fn.onOptionClick = function(){
		// form object
		let form = $(this).closest('form');
		
		let index = $(this).closest('.form-group').index();
		// set form input selected value
		form.find('.form-group').eq(index).find('input.product-option').val($(this).val());	
		form.find('.form-group').each(function(i){	
			if (i > index){
				$(this).find('input.product-option').val('');
			}
		});
		
		
		
		//set options
		//form.selectOptions($.data('options-cache'));
		form.selectOptions();
		// add/remove class active
		$(this).parent().find('li').each(function(){
			$(this).removeClass('active');
		});
		$(this).addClass('active');
		
		//get the props
		var chosen = $(this).data('property-name').split(/\s/);
		
		// show/hide more variant info box
		var show = false;
		if (chosen){
			$('.more-variant-info').find('.prop').each(function(){	
				let show_this = false;
				let prop = $(this);
				$.each(chosen, function(i, v){							
					if (prop.data('for').includes(v)) {
						show_this = true;
					} 
				});
				if (show_this) {
					$(this).show();
					show = true;
				} else {
					$(this).hide();				
				}
			});
			if (show){
				$('.more-variant-info').show();			
			} else {
				$('.more-variant-info').hide();
			}			
		}	
	};
	
	$('ul.product-option li').on('click', function(){
		$(this).onOptionClick();
	});
	
	
	//plus-minus qty set
	//plus-minus on click
	$('.product').on("click", ".plus, .minus", function() {
				
                var t = jQuery(this),
                  e = t.closest(".qty").find('input[name="quantity"]'),
                  i = parseFloat(e.val()),
                  o = parseFloat(e.attr("max")),
                  r = parseFloat(e.attr("min")),
                  a = e.attr("step");

                i && "" !== i && "NaN" !== i || (i = 0),
                "" !== o && "NaN" !== o || (o = ""),
                "" !== r && "NaN" !== r || (r = 0),
                "any" !== a && "" !== a && void 0 !== a && "NaN" !== parseFloat(a) || (a = 1),
                t.is(".plus") ? o && (o === i || i > o) ? e.val(o) : e.val(i + parseFloat(a)) : r && (r === i || i < r) ? e.val(r) : i > 0 && e.val(i - parseFloat(a)),
                e.trigger("change");
	});	
	
	//init
	//$('.product-form').customProductForm();
	//$.data('options-cache', {});
	//var optionsCache = {};
	
	
})(jQuery, window, document);